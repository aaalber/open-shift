require 'sinatra'

set :port, 8080
set :static, true
set :public_folder, "static"
set :views, "views"

get '/' do
    return 'Hello world'
end

get '/build/' do
    erb :build_form
end

post '/build/' do
    hostname = params[:hostname] 
    macaddr = params[:macaddr]
    os = params[:os]

#    greeting = params[:greeting] || "Hi There"
#    name = params[:name] || "Nobody"
   


    erb :index, :locals => {'hostname' => hostname, 'macaddr' => macaddr, 'os' => os}
end
